const express = require('express');
const cors = require('cors');
const Sequelize = require('sequelize');

const PORT = 5000;

const app = express();


const sequelize = new Sequelize('CPPD', 'username', 'password', {
  host: 'localhost',
  dialect: 'sqlite',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  // SQLite only
  storage: 'path/to/database.sqlite'
});


// Lista teste de funcionários
/* const funcionarios = [
    {
        nome: "MARCELO FERNANDEZ CARDILLO DE MORAIS URANI",
        matricula: "1066763",
        cargo: "Professor",
        classe: "Denominação Assistente Nível I"
    },
    {
        nome: "CARLOS EDUARDO COELHO FREIRE BATISTA",
        matricula: "1545948",
        cargo: "Professor",
        classe: "Classe A denominação Adjunto NÍVEL I"
    }, 
    {
        nome: "ROSSANA CRISTINA HONORATO DE OLIVEIRA",
        matricula: "2306954",
        cargo: "Professor",
        classe: "Classe B, de Professor ASSISTENTE NÍVEL I"
    }
];
 */
app.use(cors());

// Rota raiz
app.get('/', (req, res) => {
    res.send("<h1></h1>");
});

/*
    Rota para o acesso da lista de funcionários.
*/
app.get('/funcionarios', (req, res) =>{
    /*
        Se algo for passado no query o bloco dentro do
        if será executado.
        Ex:
        Rota: /funcionarios?name=Carlos
        req.query: { name: 'Carlos' }

        Rota: /funcionarios?name=Carlos&&cargo=Professor
        req.query: { name: 'Carlos', cargo: 'Professor' }
        @TODO
        Pesquisa no BD por dados passado pelo query
    */
   
    if( Object.keys(req.query).length !== 0){
        
    }else{
        
    }
});

// Porta de escuta PORT
app.listen(PORT, _ => console.log(`Server listening on port ${PORT}`));