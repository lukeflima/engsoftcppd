const express = require('express');
const cors = require('cors');
const sqlite3 = require('sqlite3').verbose();

const PORT = 5000;

const app = express();
/*
    Template para implementação do banco de dados
    @TODO
    Conectar com banco de dados
*/
const initDB = () =>  
    db = new sqlite3.Database('../Database/CPPD.db', (err) => {
    if (err) {
      return console.error(err.message);
    }
    console.log('Connected to the in-memory SQlite database.');
});

// Lista teste de funcionários
/* const funcionarios = [
    {
        nome: "MARCELO FERNANDEZ CARDILLO DE MORAIS URANI",
        matricula: "1066763",
        cargo: "Professor",
        classe: "Denominação Assistente Nível I"
    },
    {
        nome: "CARLOS EDUARDO COELHO FREIRE BATISTA",
        matricula: "1545948",
        cargo: "Professor",
        classe: "Classe A denominação Adjunto NÍVEL I"
    }, 
    {
        nome: "ROSSANA CRISTINA HONORATO DE OLIVEIRA",
        matricula: "2306954",
        cargo: "Professor",
        classe: "Classe B, de Professor ASSISTENTE NÍVEL I"
    }
];
 */
app.use(cors());

// Rota raiz
app.get('/', (req, res) => {
    res.send("<h1>PROGRESSO</h1>");
});

/*
    Rota para o acesso da lista de funcionários.
*/
app.get('/funcionarios', (req, res) =>{
    /*
        Se algo for passado no query o bloco dentro do
        if será executado.
        Ex:
        Rota: /funcionarios?name=Carlos
        req.query: { name: 'Carlos' }

        Rota: /funcionarios?name=Carlos&&cargo=Professor
        req.query: { name: 'Carlos', cargo: 'Professor' }
        @TODO
        Pesquisa no BD por dados passado pelo query
    */
    console.log("req.query", req.query);
    if( req.query.matricula){
        const db = initDB();
        let sql = `SELECT *
                    FROM Professor
                    WHERE Matricula = ?`;
        const matricula = req.query.matricula;
        db.get(sql, [matricula], (err, row) => {
            if (err) { 
                return console.error(err.message);
            }
            res.json(row);
            //console.log(row);
            return row;    
        });
        db.close();
        //res.json(req.query);
       // console.log(req.query);
    }else{
    /*
        @TODO
        Obter do banco de dados a lista de funcionarios
        e retornar o JSon por app.send.
    */
        const db = initDB();
        let sql = `SELECT *
                FROM Professor`;

        db.all(sql, [], (err, row) => {
            if (err) { 
                return console.error(err.message);
            }
            res.json(row);
            //console.log(row);
            return row;    
        });
        db.close();
    }
});

app.get('/departamentos', (req, res) =>{
    /*
        Se algo for passado no query o bloco dentro do
        if será executado.
        Ex:
        Rota: /funcionarios?name=Carlos
        req.query: { name: 'Carlos' }

        Rota: /funcionarios?name=Carlos&&cargo=Professor
        req.query: { name: 'Carlos', cargo: 'Professor' }
        @TODO
        Pesquisa no BD por dados passado pelo query
    */
    console.log("req.query", req.query);
    if( req.query.matricula){
        const db = initDB();
        let sql = `SELECT *
                    FROM Professor
                    WHERE ID = ?`;
        const matricula = req.query.matricula;
        db.get(sql, [matricula], (err, row) => {
            if (err) { 
                return console.error(err.message);
            }
            res.json(row);
            //console.log(row);
            return row;    
        });
        db.close();
        //res.json(req.query);
       // console.log(req.query);
    }else{
    /*
        @TODO
        Obter do banco de dados a lista de funcionarios
        e retornar o JSon por app.send.
    */
        const db = initDB();
        let sql = `SELECT *
                FROM Departamento`;

        db.all(sql, [], (err, row) => {
            if (err) { 
                return console.error(err.message);
            }
            res.json(row);
            //console.log(row);
            return row;    
        });
        db.close();
    }
});

/*
    Adiciona professor ao BD
    Ex:
        http://localhost:5000/add?tabela=Professor&&nome=Lucas%20Lima&&matricula=1654125&&classe=A&&nivel=1
        req.query = { tabela: 'Professor',
                      nome: 'Lucas Lima',
                      matricula: '1654125',
                      classe: 'A',
                      nivel: '1' }
        `http://localhost:5000/add?tabela=Departamento&&nome=${nome}&&ID=${id}&&sigla={sigla}`
        `http://localhost:5000/add?tabela=Modelo%20Padrao&&Processo=${processo}&&Matricula=${matricula}&&Departamento=${departamento}&&ClasseOriginal=${classeOriginal}&&NivelOriginal=${nivelOriginal}&&DataEmissao=datetime('now')`
    *Todas os atributos devem ser passados nessa ordem 
*/
app.get('/add', (req, res) =>{
    console.log(req.query);
    const table = req.query.tabela;
    delete req.query.tabela;
    const data = Object.values(req.query);
    const campos = Object.keys(req.query)
                    .map(q => q.replace(/^./, q[0].toUpperCase()));
    
    const db = initDB();
	let sql = `INSERT INTO [${table}] (`;
	campos.forEach(element =>
		sql = sql + element+ `,`
	);
	sql = sql.slice(0,-1);
	sql = sql + `) `; 
	sql = sql + ` VALUES(`;
	campos.forEach( _ =>
		sql = sql + `?,`
	);
	sql = sql.slice(0,-1);
	sql = sql + `)`;
	db.run(sql, data, err => {
		if (err) {
            res.send(err.message);
            return console.error(err.message);
		}else{
            console.log('Insert success XD');
            db.get(`SELECT last_insert_rowid() FROM [${table}]`,[],(err,row) =>{
                if (err) {
                    res.send(err.message);
                    return console.error(err.message);
                }else{
                    console.log(row);
                    res.send({id: row['last_insert_rowid()']});
                    return row;
                }

            });
            
        }
	});
});
/*
    Atualiza a(s) informação(ões) de um professor no BD
    Ex: http://localhost:5000/update?tabela=professor&&classe=B&&campo2=Matricula&&vl=415984
    req.query = { tabela: 'professor',
                  classe: 'B',
                  campo2: 'Matricula',
                  vl: '415984' }
   *A única exigência é que vl seja o último atributo               
*/
app.get('/update', (req, res) =>{
    console.log(req.query);
    const table = req.query.tabela;
    delete req.query.tabela;
    const campo2 = req.query.campo2;
    delete req.query.campo2;
    const data = Object.values(req.query);
    //console.log(data);
    delete req.query.vl;
    const campos = Object.keys(req.query)
                    .map(q => q.replace(/^./, q[0].toUpperCase()));
    //console.log(campos);
    const db = initDB();
	let sql = `UPDATE [${table}] SET `;
	campos.forEach(element =>{
		sql = sql + element+ ` = ?,`;
	});
	sql = sql.slice(0,-1);
	sql = sql + ` WHERE ` + campo2+` = ?`;
    
    console.log(sql);        
	
	db.run(sql, data, err => {
        if (err) {
            res.send(err.message);  
            return console.error(err.message);
        }else{
            console.log(`Row(s) updated: ${this.changes}`);
            res.send(`Row(s) updated: ${this.changes}`);
        }    
    });
});
/*
    Deleta uma linha do banco de dados
    Ex: http://localhost:5000/delete?tabela=professor&&matricula=415984
    req.query = { tabela: 'professor', matricula: '415984' }
*/    
app.get('/delete', (req, res) =>{
    console.log(req.query);
    const table = req.query.tabela;
    delete req.query.tabela;
    const data = Object.values(req.query);
    const campo = Object.keys(req.query)
                    .map(q => q.replace(/^./, q[0].toUpperCase()));
    const db = initDB();                
    db.run(`DELETE FROM [${table}] WHERE [${campo}] =?`, data, err => {
        if (err) {
            res.send(err.message);  
            return console.error(err.message);
        }else{
            console.log(`Row(s) deleted ${this.changes}`);
            res.send(`Row(s) deleted ${this.changes}`);
        }
    });
    

});

app.get('/search',(req,res) =>{
    if(req.query.dataemissao){
        date = req.query.dataemissao;
        //console.log(date);
        date2 = date.split("-")
        sql = `SELECT * FROM [MODELO PADRAO] `+ 
        `WHERE strftime('%m', DataEmissao) = '${date2[1]}' AND strftime('%Y', DataEmissao) = '${date2[0]}'`;
        //sql = 'SELECT rowid FROM [MODELO PADRAO]';
        const db = initDB();
        db.all(sql,[],(err ,row)=>{
            if (err) {
                res.send(err.message);  
                return console.error(err.message);
            }else{
                console.log(row);
                res.send(row);
            }

        });

    }
});

// Porta de escuta PORT
app.listen(PORT, _ => console.log(`Server listening on port ${PORT}`));