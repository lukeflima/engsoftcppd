import React, { Component } from 'react';
import { FormGroup, Radio, Form } from 'react-bootstrap';
import { Card, CardBody, Button } from 'reactstrap';
import './modeloRadio.css';
import {
	withRouter
} from 'react-router-dom';


class modelosRadio extends Component {
    constructor(props) {
        super(props)

        this.state = {
            select: ""
        } 
    }

    onChange = (event) => {
        this.setState({select: event.target.id})
        //console.log(event.target)
    }
    
    confirm = (event) => {
        console.log(event.target)
    }

    submitForm = (e) => {
        e.preventDefault();
        if(this.state.select)
		    this.props.history.push(`/Modelo${this.state.select}`);
	}

    render() {
        return(
            <Form onSubmit={this.submitForm.bind(this)}>
                <FormGroup className="list-group FormGroup">
                    <Card>
                        <CardBody>
                            <Radio
                                className="list-group-item"
                                name="Modelo"
                                onClick={this.onChange}
                                id = "A"
                            >
                                PROGRESSÕES (PROGRESSÕES E PROMOÇÕES)
                            </Radio>
                            <Radio
                                className="list-group-item"
                                name="Modelo"
                                onClick={this.onChange}
                                id="B"
                            >
                                MUDANÇA DE REGIME DE TRABALHO
                            </Radio>
                            <Radio
                                className="list-group-item"
                                name="Modelo"
                                onClick={this.onChange}
                                id="C"
                            >
                                APROVAÇÃO DE ESTÁGIO PROBATÓRIO DOCENTE
                            </Radio>
                            
                        </CardBody>
                    </Card>
                    <div 
                        className="buttondiv"
                    >
                        <Button
                            className="buttonRight"
                            size="md"
                            onClick={this.confirm}
                            type="submit"
                        >
                            Continuar
                        </Button>{' '}
                    </div>
                </FormGroup>
            </Form>
                
                
        );


    }
};

/**
  
    const group = 
    {
        width: 'auto',
        height: 'auto',
        display: 'flex',
        flexWrap: 'nowrap',
        flexDirection: 'row',
    };

 * <FormGroup className="list-group"
                    <Radio className="list-group-item" name="radioGroup" >
                        Progressões (Progressões e Promoções)
                    </Radio>
                    <Radio className="list-group-item" name="radioGroup" >
                        Modelo B
                    </Radio>
                    <Radio className="list-group-item" name="radioGroup" >
                        Modelo C
                    </Radio>
                    <Button className="button" style={{ float: 'right', margin: 20 }} type="submit">Submit</Button>
                </FormGroup>
            </div>
 */

export default withRouter(modelosRadio);