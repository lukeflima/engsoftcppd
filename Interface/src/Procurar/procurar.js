import React, { Component } from 'react';
import { Card, CardBody, Button, Form, FormGroup, Label, Input } from 'reactstrap';

class Procurar extends Component {
    constructor(props) {
        super(props);
        this.confirm = this.confirm.bind(this);
        this.state = {
            modelos: []
        }
    }

    async confirm(){
        const res = await fetch("")
        const modelos = await res.json()
        const profs = modelos.map( async (modelo) =>{
            const res = await fetch(`http://localhost:5000/funcionarios?matricula=${modelo.Professor}`)
            const prof = await res.json();
            return prof;
        })

    }

    render() {

        const {modelos} = this.state;
        const listaDeModelos = modelos.map(modelo => <li key={modelo.Processo} >{modelo.Processo}</li>)

        return (
            <div>
                <Form>
                    <Card>
                        <CardBody>
                            <FormGroup className="list-group FormGroup">
                                <Label for="De">De</Label>
                                <Input
                                    placeholder="De"
                                    id="De"
                                    className="form-control"
                                    pattern="[A-zÀ-ú]*"
                                    type="date"
                                    onChange={(e) =>{
                                        e.persist();
                                        this.setState(s =>({de: e.target.value}))
                                    }}
                                 />    
                                <Label for="Até">Até</Label>
                                <Input
                                    placeholder="Até"
                                    id="Até"
                                    className="form-control"
                                    pattern="[A-zÀ-ú]*"
                                    type="date"
                                    onChange={(e) =>{
                                        e.persist();
                                        this.setState(s =>({ate: e.target.value}))
                                    }}
                                 />
                                 <Button
                                        className="buttonRight button"
                                        size="md"
                                        onClick={this.confirm}
                                    >
                                        Pesquisar
                                </Button>{' '}  
                            </FormGroup> 
                            <ul>{listaDeModelos}</ul>
                        </CardBody>
                    </Card>
                </Form>
            </div>
        )
    }
}

export default Procurar;