import React, { Component } from 'react';
import { Radio } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { Card, CardBody, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Autosuggest from 'react-autosuggest';

import './pag2progressao.css';

import BackButton from '../BackButton/backbutton'

class modelosRadio extends Component {
    constructor(props) {
        super(props)

        this.state = {
            profs: [],
            departamentos: [],
            nome: '',
            nomeSugestao: [],
            matricula: '',
            matriculaSugestao: [],
            departamento: '',
            departamentoSugestao: [],
            modelo: {},
            nivel: '',
            classe: ''
        }
    }

    async componentDidMount() {
        console.log(this.props);
        if(!this.props.profs.size){
            console.log("Not fetched")
            try{
                const res1 = await fetch("http://localhost:5000/funcionarios");
                const profs = await res1.json();
                const res2 = await fetch("http://localhost:5000/departamentos");
                const departamentos = await res2.json();
                //console.log(profs);
                this.setState({profs, departamentos})
              } catch (e){
                console.log(e)
              }
        }else{
            this.setState({profs: this.props.profs})
        }
    }
    
    onChange = (event) => {
        //console.log(event.target)
        event.persist();
        this.setState(s =>({ 
            modelo: 
                {
                    ...s.modelo,
                    id: event.target.id 
                }
        }))
    }
    
    confirm = async () => {
        const {matricula, idDepartamento, classe, nivel} = this.state;
        if(!matricula && !idDepartamento && !classe && !nivel ) return;
        const departamento = idDepartamento;
        let idProcesso = await fetch(`http://localhost:5000/add?tabela=Modelo%20Padrao&&Matricula=${matricula}&&Departamento=${departamento}&&ClasseOriginal=${classe}&&NivelOriginal=${nivel}&&DataEmissao=${new Date().toISOString().slice(0, 19).replace('T', ' ')}`)
        idProcesso = await idProcesso.json()
        idProcesso = idProcesso.id

        this.setState(s =>(
            { 
            modelo: 
                {
                    ...s.modelo,
                    nome: this.state.nome,
                    matricula: this.state.matricula,
                    modelo: "Modelo A",
                    departamento: this.state.departamento,
                    idProcesso: idProcesso
                }
        }),
        () => {
            console.log(this.state.matricula)
            console.log(this.state.nome)
            console.log(this.state.modelo);
            this.props.setModelo(this.state.modelo);
            this.props.history.push('/Relatorio');
        })
        
        
    }

    escapeRegexCharacters(str) {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }
      
    getSuggestions(value, id) {
        const escapedValue = this.escapeRegexCharacters(value.trim());
        const regex = new RegExp('^' + escapedValue, 'i');
        let data = null
        if(id === 'P') data = this.state.profs
        if(id === 'D') data = this.state.departamentos
        return data.filter(user => regex.test(user.Nome) || regex.test(user.Matricula) || regex.test(user.Sigla));
    }
    

    render() {
        const { 
            nome, 
            nomeSugestao, 
            matricula, 
            matriculaSugestao,
            departamento,
            departamentoSugestao,
        } = this.state;

        const nomeInputProps = {
            placeholder: "Nome",
            id: "Nome",
            value: nome,
            onChange: (_, { newValue }) => this.setState({ nome: newValue}),
            className: "form-control",
            pattern: "[A-zÀ-ú]*",
            type: "text"
        };

        const emailInputProps = {
            placeholder: "Matricula",
            id: "Matricula",
            value: matricula,
            onChange: (_, { newValue }) => this.setState({ matricula: newValue.toString()}),
            className: "form-control",
            type: "number"
            
        };

        const departamentoInputProps = {
            placeholder: "Departamento",
            id: "Departamento",
            value: departamento,
            onChange: (_, { newValue }) => this.setState({ departamento: newValue}),
            className: "form-control",
            type: "text"
            
        };
        
        return (
            <div>
                <BackButton />
                <Form>
                    <Card>
                        <CardBody>
                        <FormGroup className="list-group FormGroup">
                                <Label for="Nome">Nome</Label>
                                <Autosuggest 
                                    suggestions={nomeSugestao}
                                    onSuggestionsFetchRequested={({ value }) =>  this.setState({nomeSugestao: this.getSuggestions(value,'P')})}
                                    onSuggestionsClearRequested={() => this.setState({nomeSugestao: []})}
                                    onSuggestionSelected={(_, { suggestion }) => this.setState({matricula: suggestion.Matricula.toString(), classe: suggestion.Classe, nivel: suggestion.Nivel})}
                                    getSuggestionValue={suggestion => suggestion.Nome}
                                    renderSuggestion={suggestion => <span>{suggestion.Matricula} - {suggestion.Nome}</span>}
                                    inputProps={nomeInputProps}
                                />
                            </FormGroup>
                            <FormGroup className="list-group FormGroup">
                                <Label for="Matricula">Matricula</Label>
                                <Autosuggest 
                                    suggestions={matriculaSugestao}
                                    onSuggestionsFetchRequested={({ value }) =>  this.setState({matriculaSugestao: this.getSuggestions(value,'P')})}
                                    onSuggestionsClearRequested={() => this.setState({matriculaSugestao: []})}
                                    onSuggestionSelected={(_, { suggestion }) => this.setState({nome: suggestion.Nome, classe: suggestion.Classe, nivel: suggestion.Nivel})}
                                    getSuggestionValue={suggestion => suggestion.Matricula}
                                    renderSuggestion={suggestion => <span>{suggestion.Matricula} - {suggestion.Nome}</span>}
                                    inputProps={emailInputProps}
                                />
                            </FormGroup>
                            {/* <FormGroup className="list-group FormGroup">
                                <Label for="Classe">Classe</Label>
                                <Input
                                    placeholder="Classe"
                                    id="Classe"
                                    className="form-control"
                                    pattern="[A-zÀ-ú]*"
                                    type="text"
                                    value={this.state.classe}
                                    onChange={(e) =>{
                                        e.persist();
                                        this.setState(s =>({modelo:{...s.modelo,processo: e.target.value}}))}}
                                 />    
                            </FormGroup> 
                            <FormGroup className="list-group FormGroup">
                                <Label for="Nivel">Nivel</Label>
                                <Input
                                    placeholder="Nivel"
                                    id="Nivel"
                                    className="form-control"
                                    pattern="[A-zÀ-ú]*"
                                    type="text"
                                    value={this.state.nivel}
                                    onChange={(e) =>{
                                        e.persist();
                                        this.setState(s =>({modelo:{...s.modelo,processo: e.target.value}}))}}
                                 />    
                            </FormGroup> 
                            <FormGroup className="list-group FormGroup">
                                <Label for="Seguinte Classe">Seguinte Classe</Label>
                                <Input
                                    placeholder="Seguinte Classe"
                                    id="Seguinte Classe"
                                    className="form-control"
                                    pattern="[A-zÀ-ú]*"
                                    type="text"
                                    onChange={(e) =>{
                                        e.persist();
                                        this.setState(s =>({modelo:{...s.modelo,processo: e.target.value}}))}}
                                 />    
                            </FormGroup> 
                            <FormGroup className="list-group FormGroup">
                                <Label for="Seguinte Nivel">Seguinte Nivel</Label>
                                <Input
                                    placeholder="Seguinte Nivel"
                                    id="Seguinte Nivel"
                                    className="form-control"
                                    pattern="[A-zÀ-ú]*"
                                    type="text"
                                    onChange={(e) =>{
                                        e.persist();
                                        this.setState(s =>({modelo:{...s.modelo,processo: e.target.value}}))}}
                                 />    
                            </FormGroup> */} 
                            <FormGroup className="list-group FormGroup">
                                <Label for="Departamento">Departamento</Label>
                                <Autosuggest 
                                    suggestions={departamentoSugestao}
                                    onSuggestionsFetchRequested={({ value }) =>  this.setState({departamentoSugestao: this.getSuggestions(value,'D')})}
                                    onSuggestionsClearRequested={() => this.setState({departamentoSugestao: []})}
                                    onSuggestionSelected={(_, { suggestion }) => this.setState({idDepartamento: suggestion.ID.toString()})}
                                    getSuggestionValue={suggestion => `${suggestion.Nome} - ${suggestion.Sigla}`}
                                    renderSuggestion={suggestion => <span>{suggestion.Nome} - {suggestion.Sigla}</span>}
                                    inputProps={departamentoInputProps}
                                />
                            </FormGroup>
                            <FormGroup className="list-group FormGroup">
                                <Label for="Processo">Processo</Label>
                                <Input
                                    placeholder="Processo"
                                    id="Processo"
                                    className="form-control"
                                    pattern="[A-zÀ-ú]*"
                                    type="text"
                                    onChange={(e) =>{
                                        e.persist();
                                        this.setState(s =>({modelo:{...s.modelo,processo: e.target.value}}))}}
                                 />    
                            </FormGroup> 
                            <FormGroup className="list-group FormGroup">
                                <Label for="Vigencia">Vigencia</Label>
                                <Input
                                    placeholder="Vigencia"
                                    id="Vigencia"
                                    className="form-control"
                                    pattern="[A-zÀ-ú]*"
                                    type="date"
                                    onChange={(e) =>{
                                        e.persist();
                                        this.setState(s =>({modelo:{...s.modelo, vigencia: e.target.value}}))}}
                                 />    
                            </FormGroup>                                
                            <FormGroup className="list-group FormGroup">
                                <Radio
                                    className="list-group-item"
                                    name="Modelo"
                                    onClick={this.onChange}
                                    id="1"
                                >
                                    Progressão horizontal da Classe A (denominações) Nível I para Nível II
                                </Radio>
                                <Radio
                                    className="list-group-item"
                                    name="Modelo"
                                    onClick={this.onChange}
                                    id="2"
                                >
                                    Progressão horizontal na Classe B denominação Assistente
                                </Radio>
                                <Radio
                                    className="list-group-item"
                                    name="Modelo"
                                    onClick={this.onChange}
                                    id="3"
                                >
                                    Progressão horizontal na Classe C denominação Adjunto
                                </Radio>
                                <Radio
                                    className="list-group-item"
                                    name="Modelo"
                                    onClick={this.onChange}
                                    id="4"
                                >
                                    Progressão Horizontal na Classe D denominação Associado
                                </Radio>
                                <Radio
                                    className="list-group-item"
                                    name="Modelo"
                                    onClick={this.onChange}
                                    id="5"
                                >
                                    Progressão Vertical da Classe A (denominação Auxiliar) nível II para a Classe B (denominação Assistente) nível I
                                </Radio>
                                <Radio
                                    className="list-group-item"
                                    name="Modelo"
                                    onClick={this.onChange}
                                    id="6"
                                >
                                    Progressão Vertical da Classe B (denominação assistente) nível II para a Classe C (denominação Adjunto) nível I
                                </Radio>
                                <Radio
                                    className="list-group-item"
                                    name="Modelo"
                                    onClick={this.onChange}
                                    id="7"
                                >
                                    Progressão Vertical da Classe C (denominação adjunto) nível IV para a Classe D (denominação associado) nível I
                                </Radio>
                                <Radio
                                    className="list-group-item"
                                    name="Modelo"
                                    onClick={this.onChange}
                                    id="8"
                                >
                                    Progressão Vertical da Classe D (denominação associado) nível IV para a Classe E (denominação Titular) nível I
                                </Radio>
                                <Radio
                                    className="list-group-item"
                                    name="Modelo"
                                    onClick={this.onChange}
                                    id="9"
                                >
                                    Aceleração da Promoção da Classe A Nível II para a Classe B ou C
                                </Radio>
                                <Radio
                                    className="list-group-item"
                                    name="Modelo"
                                    onClick={this.onChange}
                                    id="10"
                                >
                                    Solicitação de Retribuição por Titulação – Professor Efetivo
                                </Radio>
                                <Radio
                                    className="list-group-item"
                                    name="Modelo"
                                    onClick={this.onChange}
                                    id="11"
                                >
                                    Solicitação de Retribuição por Titulação – Professor Substituto
                                </Radio>

                                <div
                                    className="buttondiv"
                                >
                                    <Button
                                        className="buttonRight button"
                                        size="md"
                                        onClick={this.confirm}
                                    >
                                        Continuar
                            </Button>{' '}
                                </div>

                            </FormGroup>
                        </CardBody>
                    </Card>
                </Form>
            </div>

        );
    }
};

export default withRouter(modelosRadio);