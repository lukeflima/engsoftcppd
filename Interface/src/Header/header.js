import React from 'react';

const header = () => (
    <header className="App-header no-print" style={{ height: 120 }}>
        <p>CPPD</p>
    </header>
);

export default header;