import React, { Component } from 'react';
import './App.css';

import Header from './Header/header';
import ModelosRadioBox from './Modelo-RadioBox/modeloRadio';
import ModeloA from './Modelo1/pag2progressao';
import ModeloB from './Modelo2/pag2mudanca';
import ModeloC from './Modelo3/pag2aprovacao';
import GeradorModelo from './GeradorModelo/geradorModelo';
import Procurar from './Procurar/procurar'

import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';

//fontawesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons'

library.add(faAngleLeft)



class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profs: [],
      modelo: {},
    };
  }

  setModelo = (modelo) => {
    this.setState({modelo});
  }

  async componentDidMount() {
    try{
      const res = await fetch("http://localhost:5000/funcionarios");
      const profs = await res.json();
      //console.log(profs);
      this.setState({profs});
    } catch (e){
      console.log(e)
    }
    
    
  }

  render() {
    const {profs, modelo} = this.state;

    return (
      <div className="App">
        <Header />
        <Router>
          <Switch>
            <Route path="/" component={ModelosRadioBox} exact />
            <Route path="/ModeloA" render={() => <ModeloA profs={profs} setModelo={this.setModelo} />} />
            <Route path="/ModeloB" render={() => <ModeloB profs={profs} setModelo={this.setModelo}/>} />
            <Route path="/ModeloC" render={() => <ModeloC profs={profs} setModelo={this.setModelo}/>} />
            <Route path="/Relatorio" render={() => <GeradorModelo modelo={modelo} />} />
            <Route path="/Procurar" component={Procurar} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
