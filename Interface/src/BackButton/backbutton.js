import React from 'react';
import { Button } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import './backbutton.css'

const backButton = (props) => (
    <Button
        className="button icon-left backbt"
        onClick={props.history.goBack}
    >
        <FontAwesomeIcon icon="angle-left" pull="left" size="lg" />
        Voltar
    </Button>  
);

export default withRouter(backButton);