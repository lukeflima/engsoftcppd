import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Button, } from 'reactstrap';
import * as jsPDF  from 'jspdf'
import html2canvas from 'html2canvas';

import './template.css';

const pxToMm = (px) => {
    return Math.floor(px/document.getElementById('myMm').offsetHeight);
  };
  
const mmToPx = (mm) => {
return document.getElementById('myMm').offsetHeight*mm;
};

const range = (start, end) => {
    return Array(end-start).join(0).split(0).map(function(val, id) {return id+start});
};

class GeradorModelo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            file: "",
            modelo: {},
        }

        this.gerarPdf = this.gerarPdf.bind(this)
    }
    componentDidMount() {
        const file = this.file
        this.setState({file})
    }

    gerarPdf(){
        const {idProcesso, processo} = this.state.modelo;
        const input = document.getElementById('pdf');
        
        const inputHeightMm = pxToMm(input.offsetHeight);
        const a4WidthMm = 210;
        const a4HeightMm = 297; 
        const a4HeightPx = mmToPx(a4HeightMm); 
        const numPages = inputHeightMm <= a4HeightMm ? 1 : Math.floor(inputHeightMm/a4HeightMm) + 1;
        console.log({
            input, inputHeightMm, a4HeightMm, a4HeightPx, numPages, range: range(0, numPages), 
            comp: inputHeightMm <= a4HeightMm, inputHeightPx: input.offsetHeight
        });
        

        html2canvas(input)
            .then((canvas) => {
            const imgData = canvas.toDataURL('image/png');
            
            let pdf = null;
            // Document of a4WidthMm wide and inputHeightMm high
            if (inputHeightMm > a4HeightMm) {
                // elongated a4 (system print dialog will handle page breaks)
                pdf = new jsPDF('p', 'mm', [inputHeightMm+16, a4WidthMm]);
            } else {
                // standard a4
                pdf = new jsPDF();
            }
            
                pdf.addImage(imgData, 'PNG', -a4WidthMm*0.33, 10);
                pdf.save(`relatorio${idProcesso}-${processo}.pdf`);
            });
    }
    componentWillMount() {
        console.log(!this.props.modelo.nome)
        if(!this.props.modelo.nome){
            this.props.history.push('/');
        }else{
            this.setState({modelo: this.props.modelo})
        }
        
    }
    render() {

        let file = "";
        const monName = ["JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO"]
        const now = new Date();
        const data = `${now.getFullYear()}, ${now.getDate()} DE ${monName[now.getMonth()]} DE ${now.getFullYear()}`
        let {id, nome, matricula,idProcesso} = this.props.modelo;
        if(nome) nome = nome.toUpperCase()
        let {vigencia, departamento, processo} = this.props.modelo;
        const doo = (new Date(vigencia))
        const vigenciaData = new Date(doo.getTime() + Math.abs(doo.getTimezoneOffset()*60000))
        vigencia = `${vigenciaData.getDate()} DE ${monName[vigenciaData.getMonth()]} DE ${vigenciaData.getFullYear()}`
        if(this.props.modelo.modelo === "Modelo A"){
            if(id === '1'){
                file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner" class="A4"> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS (PROGEP) DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto, e considerando o que consta do Processo n.º ${processo}, resolve:</p></br> <p class="corpo">Conceder Progressão Funcional Horizontal da Classe A denominação Adjunto NÍVEL I para a Classe A Denominação Adjunto NÍVEL II, a ${nome}, matricula do SIAPE n.º ${matricula}, lotado(a) no ${departamento} desta Universidade, de acordo com o disposto nas e das Leis 12.772 de 28.12.2012, Lei 12.863, de 24 de 24.09.2013 e Resolução 027/2001 CONSEPE .</p><p class="corpo negrito">Na Classe A Denominação Adjunto NÍVEL II , com vigência a partir de ${vigencia}.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
            }
            if(id === '2'){
                file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS (PROGEP) DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto, e considerando o que consta do Processo n.º ${processo}, resolve:</p></br> <p class="corpo">Conceder Progressão Funcional Horizontal da Classe B, de Professor ASSISTENTE NÍVEL I (501) para a Classe B, de Professor ASSISTENTE NÍVEL II (502), a ${nome}, matricula do SIAPE n.º ${matricula}, lotado(a) no ${departamento}desta Universidade, de acordo com o disposto nas e das Leis 12.772 de 28.12.2012, Lei 12.863, de 24 de 24.09.2013 da Resolução Nº 037/1999 e Resolução 027/2001, do CONSEPE-UFPB.</p><p class="corpo negrito">Na Classe A Denominação Adjunto NÍVEL II , com vigência a partir de ${vigencia}.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
            }
            if(id === '3'){
                file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS -PROGEP- DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto Geral e considerando o que consta do Processo n.º ${processo}, RESOLVE:</p></br> <p class="corpo">Conceder Progressão Funcional Horizontal de Professor ADJUNTO NÍVEL II (602), Classe C, para Professor ADJUNTO, NÍVEL III (603), Classe C, a ${nome}, matricula do SIAPE n.º ${matricula}, lotado(a) no ${departamento}, desta Universidade, de acordo com o disposto na Leis 12.772, da Resolução Nº 37/1999 e resolução 027/2001, do CONSEPE-UFPB,</p><p class="corpo negrito">com vigência a partir de ${vigencia}.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
            }
            if(id === '4'){
                file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS -PROGEP- DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto Geral e considerando o que consta do Processo n.º ${processo}, RESOLVE:</p></br> <p class="corpo">Conceder Progressão Funcional Horizontal Professor(a) ASSOCIADO NÍVEL I(701), Classe D, para Professor ASSOCIADO NÍVEL II(702), Classe D, a ${nome}, matricula do SIAPE n.º ${matricula}, lotado(a) no ${departamento}desta Universidade, de acordo com o disposto nas e das Leis 12.772, da Resolução 28.12.2012, e a Resolução nº 54/2006 e 14/2008 do CONSEPE.</p><p class="corpo negrito">Professor ASSOCIADO NÍVEL II(702), Classe D ,com vigência a partir de ${vigencia}</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
            }
            if(id === '5'){
                file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS (PROGEP) DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto Geral e considerando o que consta do Processo n.º ${processo}, resolve:</p></br> <p class="corpo">Conceder Progressão Funcional vertical da Classe A, de Professor AUXILIAR NÍVEL II(402), para a Classe B, de Professor ASSISTENTE NÍVEL I(501), a ${nome}, matricula do SIAPE n.º ${matricula}, lotado(a) no ${departamento}desta Universidade, de acordo com o disposto nas e das Leis 12.772, da Resolução 28.12.2012, Lei 12.863, de 24 de 24.09.2013 e a Resolução 027/1001 CONSEPE.</p><p class="corpo negrito">Na Classe B, de Professor Assistente NÍVEL I(501), com vigência a partir de ${vigencia}.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
            }
            if(id === '6'){
                file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS (PROGEP) DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto Geral e considerando o que consta do Processo n.º ${processo}, resolve:</p></br> <p class="corpo">Conceder Progressão Funcional vertical da Classe B, de Professor ASSISTENTE NÍVEL II(502) para a Classe C, de Professor ADJUNTO NÍVEL I(601), a ${nome}, matricula do SIAPE n.º ${matricula}, lotado(a) no ${departamento}, desta Universidade, de acordo com o disposto nas Leis 12.772 de 28.12.2012, Lei 12.863, de 24.09.2013 da Resolução Nº 027/2001 do CONSEPE.</p><p class="corpo negrito">Na Classe C, de Professor ADJUNTO NÍVEL I(601), com vigência a partir de ${vigencia}.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
            }
            if(id === '7'){
                file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS -PROGEP- DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto Geral e considerando o que consta do Processo n.º ${processo}, RESOLVE:</p></br> <p class="corpo">Conceder Progressão Funcional Vertical Professor(a) ADJUNTO NÍVEL IV(604), Classe C, para Professor ASSOCIADO NÍVEL I(701), Classe D, a ${nome}, matricula do SIAPE n.º ${matricula}, lotado(a) no ${processo}, desta Universidade, de acordo com o disposto nas e da Lei 12.772 de 28.12.2012 e a Resolução nº 54/2006 e 15/2006 e 14/2008 do CONSEPE.</p><p class="corpo negrito">Professor ASSOCIADO NÍVEL I (701), Classe D, com vigência a partir de ${vigencia}.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
            }
            if(id === '8'){
                file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS -PROGEP- DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto Geral e considerando o que consta do Processo n.º ${processo}, resolve:</p></br> <p class="corpo">Conceder Progressão Funcional Vertical da Classe D, de Professor ASSOCIADO NÍVEL IV(704) para a Classe E, de Professor TITULAR NÍVEL I (801), a ${nome}, matricula do SIAPE n.º ${matricula}, lotado(a) no ${departamento}desta Universidade, de acordo com o disposto nas e da Lei 12.772 de 28.12.2012, 12.863 de 24.09.2013 e Resolução 33/2014 do CONSEPE.</p><p class="corpo negrito">Na Classe E, de Professor TITULAR NÍVEL I (801), com vigência a partir de ${vigencia}.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
            }
            if(id === '9'){
                file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS -PROGEP- DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto Geral e considerando o que consta do Processo n.º ${processo}, resolve:</p></br> <p class="corpo">Conceder Aceleração da Promoção de Classe A denominação Assistente Nível II para Classe B, ASSISTENTE Nível I (501), com a RT de MESTRE, a ${nome}, matricula do SIAPE n.º ${matricula}, lotado(a) no ${departamento}, desta Universidade, amparado nas Leis 12.772, de 28.12.2012, Lei 12.863, de 24 de 24.09.2013, nas Resoluções 37/99 e 005/2002 do CONSEPE.</p><p class="corpo"> Com vigência e os efeitos financeiros a partir de ${vigencia}.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
            }
            if(id === '10'){
                file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS -PROGEP- DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto Geral e considerando o que consta do Processo n.º ${processo}, resolve:</p></br> <p class="corpo">Conceder Retribuição de Titulação de DOUTOR, a ${nome}, matricula do SIAPE n.º ${matricula}, Professor Classe C NÍVEL I, lotado(a) ${departamento}, desta Universidade, amparado nas Leis 12.772, de 28.12.2012, Lei 12.863, de 24 de 24.09.2013, nas Resoluções 37/99 e 005/2002 do CONSEPE.</p><p class="corpo"> Com vigência e os efeitos financeiros a partir de ${vigencia}.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
            }
            if(id === '11'){
                file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS -PROGEP- DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto Geral e considerando o que consta do Processo n.º ${processo}, resolve:</p></br> <p class="corpo">Conceder Retribuição de Titulação de MESTRE, a ${nome}, professora(a) Substituto(a) da Classe A - NÍVEL I, matricula do SIAPE n.º ${matricula}, lotado(a) ${departamento}, desta Universidade, amparado pelas Lei 12.772 de 28.12.2012, Lei 12.863 de 24.09.2013, Resoluções 37/99 e 005/2002 do CONSEPE.</p><p class="corpo"> Na Classe A - NÍVEL I ( Substituto ) com RT de MESTRE e os efeitos financeiros a partir de ${vigencia}.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
            }        
        }
        if(this.props.modelo.modelo === "Modelo B"){
            file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS -PROGEP- DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto Geral e considerando o que consta do Processo n.º ${processo}, resolve:</p></br> <p class="corpo">Conceder a Mudança de Regime de Trabalho de T–40 (QUARENTA HORAS SEMANAIS) para T–40 (QUARENTA HORAS SEMANAIS) COM DEDICAÇÃO EXCLUSIVA a ${nome}, matricula do SIAPE n.º ${matricula}, Classe B , ASSISTENTE, NÍVEL I(501), lotado(a) no ${departamento}, desta Universidade, de acordo com os critérios constantes da Resolução No 18/99 do CONSUNI, Resolução Nº 64/2009 CONSEPE e Lei n° 12.772/2012 alterada pela Lei n° 12.863/2013, com vigência a partir de ${vigencia}.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
        }
        if(this.props.modelo.modelo === "Modelo C"){
            const {classe, nivel} = this.props.modelo
            file = `<link rel="stylesheet" type="text/css" href="template.css"><div id="conteiner"> </br> </br> <div class="no-margin negrito"> <p>SERVIÇO PÚBLICO FEDERAL</p><p>UNIVERSIDADE FEDERAL DA PARAÍBA</p><p>PRÓ-REITORIA DE GESTÃO DE PESSOAS</p><p>COMISSÃO PERMANENTE DE PESSOAL DOCENTE</p></div></br> </br> <p class="negrito">PORTARIA PROGEP/CPPD/Nº ${idProcesso}/${data}.</p></br> <p class="corpo">O PRÓ-REITOR DE GESTÃO DE PESSOAS -PROGEP- DA UNIVERSIDADE FEDERAL DA PARAÍBA, usando da atribuição que lhe confere o artigo 42, alínea b, do Estatuto Geral e considerando o que consta do Processo n.º ${processo},bem como, de acordo com o Parecer da Comissão Permanente do Pessoal Docente-CPPD-UFPB e da legislação vigente, resolve:</p></br> <p class="corpo">APROVAR o relatório conclusivo do Estágio Probatório de suas atividades acadêmicas, desenvolvidas nos Períodos Letivos, compreendidos de ${vigencia}, do (a) Professor (a) ${nome}, matricula do SIAPE n.º ${matricula}, ${classe}, de Professor (a) ${nivel}, lotado(a) no ${departamento}, desta Universidade, de acordo com o disposto na Resolução No 05/99, do CONSUNI-UFPB.</p></br> </br> <div class="no-margin"> <p class="negrito">FRANCISCO RAMALHO DE ALBUQUERQUE</p><p class="negrito">PRÓ-REITOR DE GESTÃO DE PESSOAS</p></div></div>`
        }
        file = `</br></br></br>` + file
        this.file = file;
        return(
            <div >
                <div id="pdf" dangerouslySetInnerHTML={{ __html: file }} />
                <Button
                    className="buttonRight button no-print"
                    size="md"
                    onClick={this.gerarPdf}
                >
                    Gerar PDF
                </Button>{' '}
                <div id="myMm" style={{height: "1mm"}} />
            </div>
        );
    }
}

export default withRouter(GeradorModelo);