--
-- File generated with SQLiteStudio v3.2.1 on dom out 14 23:20:30 2018
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Departamento
CREATE TABLE Departamento (
    Nome  CHAR,
    ID    INTEGER NOT NULL
                  PRIMARY KEY AUTOINCREMENT,
    Sigla CHAR
);


-- Table: Modelo A
CREATE TABLE [Modelo A] (
    NovaClasse   CHAR    CONSTRAINT ClasseInvalida CHECK (NovaClasse IN ('A', 'B', 'C', 'D', 'E') ),
    NovoNivel    CHAR    CONSTRAINT NivelInvalido CHECK (NovoNivel IN ('1', '2', '3', '4', '5') ),
    DataVigencia DATE,
    Processo     INTEGER PRIMARY KEY
                         REFERENCES [Modelo Padrão] (Processo),
    LeiProcesso  CHAR
);


-- Table: Modelo B
CREATE TABLE [Modelo B] (
    ClasseE      CHAR       CONSTRAINT ClasseInvalida CHECK (ClasseE IN ('E') ),
    Aprovacao    CHAR (100),
    DataVigencia DATE,
    Processo     INTEGER    PRIMARY KEY
                            REFERENCES [Modelo Padrão] (Processo),
    LeiProcesso  CHAR
);


-- Table: Modelo C
CREATE TABLE [Modelo C] (
    NovaClasse   CHAR       CONSTRAINT ClasseInvalida CHECK (NovaClasse IN ('A', 'B', 'C', 'D', 'E') ),
    NovoNivel    CHAR       CONSTRAINT NivelInvalido CHECK (NovoNivel IN ('1', '2', '3', '4', '5') ),
    MotivoSalto  CHAR (100),
    DataVigencia DATE,
    Processo     CHAR       PRIMARY KEY
                            REFERENCES [Modelo Padrão] (Processo),
    LeiProcesso  CHAR
);


-- Table: Modelo D
CREATE TABLE [Modelo D] (
    NovoTitulo     CHAR,
    Universidade   CHAR,
    DataVigencia   DATE,
    Processo       CHAR PRIMARY KEY
                        REFERENCES [Modelo Padrão] (Processo),
    LeiProcesso    CHAR,
    TituloOriginal CHAR
);


-- Table: Modelo E
CREATE TABLE [Modelo E] (
    RegimeAntigo CHAR,
    RegimeNovo   CHAR,
    DataVigente  DATE,
    Processo     CHAR PRIMARY KEY
                      REFERENCES [Modelo Padrão] (Processo),
    LeiProcesso  CHAR
);


-- Table: Modelo F
CREATE TABLE [Modelo F] (
    PedidoRegime CHAR,
    Relatorio    CHAR,
    DataInicio   DATE,
    DataTermino  DATE,
    Resolucao    CHAR,
    Processo     CHAR NOT NULL
                      PRIMARY KEY
                      REFERENCES [Modelo Padrão] (Processo) 
);


-- Table: Modelo Padrao
CREATE TABLE [Modelo Padrao] (
    Processo       INTEGER NOT NULL
                           PRIMARY KEY AUTOINCREMENT,
    Matricula      INTEGER NOT NULL,
    Departamento   CHAR    NOT NULL,
    ClasseOriginal CHAR    CONSTRAINT ClasseInvalida CHECK (ClasseOriginal IN ('A', 'B', 'C', 'D', 'E') ),
    NivelOriginal  CHAR    CONSTRAINT NivelInvalido CHECK (NivelOriginal IN ('1', '2', '3', '4', '5') ),
    DataEmissao    DATE,
    FOREIGN KEY (
        Departamento
    )
    REFERENCES Departamento (ID),
    FOREIGN KEY (
        Matricula
    )
    REFERENCES Professor (Matricula) 
);


-- Table: Professor
CREATE TABLE Professor (
    Nome      CHAR,
    Matricula INTEGER NOT NULL
                      PRIMARY KEY,
    Classe    CHAR    CONSTRAINT ClasseValida CHECK (Classe IN ('A', 'B', 'C', 'D', 'E') ),
    Nivel     CHAR    CONSTRAINT NivelValido CHECK (Nivel IN ('1', '2', '3', '4', '5') ) 
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
