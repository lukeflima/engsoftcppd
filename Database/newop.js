const sqlite3 = require('sqlite3').verbose();
 
// conecta com o BD  
let db = new sqlite3.Database('teste.db',(err) =>{
	if (err) {
		return console.error(err.message);
	}
	console.log('Connected to the DB XD');
});
 

//cria a tabela
function criarTabela(){

	
	//tabelas de modelo 1 a 8
	db.serialize(() => {
		//let sql0 = "PRAGMA foreign_keys = on";
		//db.run(sql0);
		db.get("PRAGMA foreign_keys = ON");
		let sql = "CREATE TABLE Departamento (\n"+
    				"Nome  CHAR,\n"+
				    "ID INTEGER PRIMARY KEY,\n"+
				    "SIGLA  CHAR \n"+
					");";
		db.run(sql);
		let sql1 = "CREATE TABLE [Modelo Padrão] (\n"+
    			"Processo       INTEGER NOT NULL PRIMARY KEY,\n"+
    			"Matricula      INTEGER NOT NULL,\n"+
    			"Departamento   CHAR NOT NULL ,\n"+
    			"ClasseOriginal CHAR    CONSTRAINT ClasseInvalida CHECK (ClasseOriginal IN ('A', 'B', 'C', 'D', 'E') ),\n"+
    			"NivelOriginal  CHAR    CONSTRAINT NivelInvalido CHECK (NivelOriginal IN ('1', '2', '3', '4', '5') ),\n"+
    			"DataEmissao    DATE,\n"+
    			"FOREIGN KEY(Matricula) REFERENCES Professor (Matricula),\n"+
    			"FOREIGN KEY(Departamento) REFERENCES Departamento (ID) \n"+
				");";
		db.run(sql1);
		
		let sql2 ="CREATE TABLE Professor (\n"+
    			"Nome      CHAR,\n"+
   				"Matricula INTEGER PRIMARY KEY,\n"+
  			  	"Classe    CHAR    CONSTRAINT ClasseValida CHECK (Classe IN ('A', 'B', 'C', 'D', 'E') ),\n"+
    			"Nivel     CHAR    CONSTRAINT NivelValido CHECK (Nivel IN ('1', '2', '3', '4', '5') ) \n"+
				");"; 		
		db.run(sql2);
		
		let sql3 = "CREATE TABLE [Modelo A] (\n"+
			    "NovaClasse   CHAR    CONSTRAINT ClasseInvalida CHECK (NovaClasse IN ('A', 'B', 'C', 'D', 'E') ),\n"+
			    "NovoNivel    CHAR    CONSTRAINT NivelInvalido CHECK (NovoNivel IN ('1', '2', '3', '4', '5') ),\n"+
			    "DataVigencia DATE,\n"+
			    "Processo     INTEGER PRIMARY KEY REFERENCES [Modelo Padrão] (Processo),\n"+
			    "LeiProcesso  CHAR \n"+
				");";

		db.run(sql3);		
		
		let sql4	=	"CREATE TABLE [Modelo B] (\n"+
				    "ClasseE                 CONSTRAINT ClasseInvalida CHECK (ClasseE IN ('E') ),\n"+
				    "Aprovação    CHAR (100),\n"+
				    "DataVigencia DATE,\n"+
				    "Processo     INTEGER    PRIMARY KEY REFERENCES [Modelo Padrão] (Processo),\n"+				                  
				    "LeiProcesso  CHAR \n"+
					");";
		db.run(sql4);			

		

		let	sql5 =	"CREATE TABLE [Modelo C] (\n"+					
					"NovaClasse   CHAR       CONSTRAINT ClasseInvalida CHECK (NovaClasse IN ('A', 'B', 'C', 'D', 'E') ),\n"+
					"NovoNivel    CHAR       CONSTRAINT NivelInvalido CHECK (NovoNivel IN ('1', '2', '3', '4', '5') ),\n"+
					"MotivoSalto  CHAR (100),\n"+
					"DataVigencia DATE,\n"+
					"Processo     CHAR       PRIMARY KEY  REFERENCES [Modelo Padrão] (Processo),\n"+					                           
					"LeiProcesso  CHAR \n"+
					");";
		db.run(sql5);		

		
		let	sql6	=	"CREATE TABLE [Modelo D] (\n"+
					"NovoTitulo     CHAR,\n"+
					"Universidade   CHAR,\n"+
					"DataVigencia   DATE,\n"+
					"Processo       CHAR PRIMARY KEY REFERENCES [Modelo Padrão] (Processo),\n"+
					"LeiProcesso    CHAR,\n"+
					"TituloOriginal CHAR \n"+
					");";


		db.run(sql6);
		let	sql7	=	"CREATE TABLE [Modelo E] (\n"+
					"RegimeAntigo CHAR,\n"+
					"RegimeNovo   CHAR,\n"+
					"DataVigente  DATE,\n"+
					"Processo     CHAR PRIMARY KEY REFERENCES [Modelo Padrão] (Processo),\n"+
					"LeiProcesso  CHAR \n"+
					");";

		db.run(sql7);				

		let sql8	=	"CREATE TABLE [Modelo F] (\n"+
					"PedidoRegime CHAR,\n"+
					"Relatorio    CHAR,\n"+
					"DataInicio   DATE,\n"+
					"DataTermino  DATE,\n"+
					"Resolucao    CHAR,\n"+
					"Processo     CHAR PRIMARY KEY REFERENCES [Modelo Padrão] (Processo)\n"+
					");";
		db.run(sql8);			
		
		//let sql9 = "PRAGMA foreign_keys = on";
		//db.run(sql9);

		
		});
	}
	
	


function inserirNaTabela(data,table,...campos){


	let sql = `INSERT INTO [${table}] (`;
	campos.forEach(function(element){
		sql = sql + element+ `,`;
	});
	sql = sql.slice(0,-1);
	sql = sql + `) `; 
	sql = sql + ` VALUES(`;
	campos.forEach(function(element){
		sql = sql + `?,`;
	});
	sql = sql.slice(0,-1);
	sql = sql + `)`;
	db.run(sql, data, function(err){
		if (err) {
			return console.error(err.message);
		}
		console.log('Insert success XD');
	});




}

//campo2 = campo pelo qual deseja pesquisar
//campos = campos que você quer editar
function atualizarTabela(data,table,campo2,...campos){
	

	let sql = `UPDATE`+ table +`SET `
	campos.forEach(function(element){
		sql = sql + element+ ` = ?,`;
	});
	sql = sql.slice(0,-1)
	sql = sql + ` WHERE ` + campo2+` = ?`;
    
    //console.log(sql);        
	
	db.run(sql, data, function(err) {
  	if (err) {
    return console.error(err.message);
 	 }
  	console.log(`Row(s) updated: ${this.changes}`);

 
	});
	


}
function deletarDaTabela(data,table,campo){
	

	db.run(`DELETE FROM`+ table +` WHERE `+ campo +`=?`, data, function(err) {
	if (err) {
	   return console.error(err.message);
	}
	console.log(`Row(s) deleted ${this.changes}`);
	});


	

}


//let dado =['Emanuella Maria', '11425394', 'B', '2', 'A', '1', 'seilahomi', '123', '2018-10-06', '2018-10-08', 'CI'];
//const sqlite3 = require('sqlite3').verbose();

db.serialize(() => {
db.get("PRAGMA foreign_keys = ON");//tem que colocar isso toda vez que for rodar uma query
//criarTabela();
//inserirNaTabela(['XD','123','A','2'],'Professor','Nome','Matricula','Classe','Nivel');
//inserirNaTabela(['Centro de informatica','1','CI'],'Departamento','Nome','ID','Sigla');
//inserirNaTabela(['458','123','Xyz','A','2','2018-10-06'],'Modelo Padrão','Processo','Matricula','Departamento','ClasseOriginal','NivelOriginal','DataEmissao');
//deletarDaTabela('Emanuella Maria','Nome');
//atualizarTabela(['11367860','XD','Emanuella Maria'],'Nome','Matricula','Nome')
	
	

/*db.get('SELECT * FROM [Modelo I-VII] ',(err,row)=>{
	if (err){
		throw err;
	}
	console.log(row);
});
*/



  //percorrer o banco e exibir tudo
/*  db.each(`SELECT * FROM [Departamento]`, (err, row) => {
    if (err) {
      console.error(err.message);
    }
    console.log(row);
  });*/

});


//});
//desconecta com o BD 
db.close((err) =>{
	if (err) {
		return console.error(err.message);
	}
	console.log('Closing connection to the DB');
});


